listsDBVar = new Mongo.Collection("listsDB");

if (Meteor.isClient) {

    Session.set('AddingCategorySessionVar',false);
    Session.set('AddingItemSessionVar',false);
    Session.set('CurrentCategorySessionVar',null);
    Session.set('AddingItemSessionVar', false);


    Template.categoryTemplate.helpers ({
        categoryIterator: function(){
            return listsDBVar.find();
        },

        isAddingCategory: function(){
            return Session.get('AddingCategorySessionVar');
        },

        btnStatus:function(){
            var tmpSessionVar = Session.get('CurrentCategorySessionVar', null);
            if(tmpSessionVar === this._id)
                return 'btn-info';
            return 'btn-primary';
        }

    });

    Template.categoryTemplate.events ({
        
        'click #btnCategoryPlusId': function(e,t){
            Session.set('AddingCategorySessionVar',true);
            Meteor.flush();
            t.find('#newCategoryText').focus();
            t.find('#newCategoryText').select();
        },

        'click #btnCategoryMinusId': function(e,t){
            var tmpSessionVar = Session.get('CurrentCategorySessionVar', null);
                if(tmpSessionVar)
                    listsDBVar.remove(tmpSessionVar);
            Session.set('CurrentCategorySessionVar', null);
            Meteor.flush();
        },

        'keyup #newCategoryText': function (e,t){
            if(e.which === 13) {
                var tmpNewStringVar = String(e.target.value || "");
                var tmpCateogoryId = listsDBVar.insert({"CategoryName": tmpNewStringVar});
                Session.set('AddingCategorySessionVar',false);
                Session.set('CurrentCategorySessionVar', tmpCateogoryId);
            }
        },

        'click .categoryClickListener': function(e,t){
            Session.set('CurrentCategorySessionVar', this._id);
        }
        
    });

    Template.itemTemplate.helpers({

        isAddingItem: function(){
            return Session.get('AddingItemSessionVar',false);
        },

        isCategorySelected: function(){
            return !(Session.get('CurrentCategorySessionVar') === null)
        },

        itemIterator: function(){
            var tmpCurrentCategoryVar = Session.get('CurrentCategorySessionVar',null);
            if(tmpCurrentCategoryVar)
                return  listsDBVar.findOne({_id:Session.get('CurrentCategorySessionVar')}).ItemsArray;
            return null;
        }
    });

    Template.itemTemplate.events({
        
        'click #btnItemPlusId': function(e,t){
            Session.set('AddingItemSessionVar',true);
            Meteor.flush();
            t.find('#newItemText').focus();
            t.find('#newItemText').select();
        },

        'keyup #newItemText': function(e,t){
            if(e.which === 13) {
                var tmpNewStringVar = String(e.target.value || "");
                var tmpCurrentCategoryVar = Session.get('CurrentCategorySessionVar',null);
                if(tmpNewStringVar && tmpCurrentCategoryVar){
                    listsDBVar.update({_id:tmpCurrentCategoryVar},
                        {$addToSet:{ItemsArray:{ItemName:tmpNewStringVar}}});
                }
                Session.set('AddingItemSessionVar',false);
            }
        },

        'focusout #newItemText': function(e,t){
            Session.set('AddingItemSessionVar',false);
        },

        'click .listItemDeleteListener':function(e,t){
            var tmpCurrentCategoryVar = Session.get('CurrentCategorySessionVar',null);
            if(tmpCurrentCategoryVar)
                listsDBVar.update({_id:tmpCurrentCategoryVar},
                    {$pull:{ItemsArray:{ItemName:e.target.id}}});
        }

    });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
